// var controller = new ScrollMagic.Controller();
// AOS.init();

// var svgstroke = new TimelineMax();

// svgstroke.to('.nuagetop',2,{strokeDashoffset:0, ease: Linear.easeNone});
// svgstroke.to('.nuagebottom',2,{strokeDashoffset:0, ease: Linear.easeNone});
// svgstroke.to('.arc',2,{strokeDashoffset:0, ease: Linear.easeNone});
// svgstroke.fromTo('.nuage',2,{opacity:0},{opacity:1});

// svgstroke.to('.pictoborder',2,{strokeDashoffset:0, ease: Linear.easeNone},"-=0.5");
// svgstroke.fromTo('.deux',2,{opacity:0},{opacity:1},"-=0.5");
// svgstroke.fromTo('.neuf',2,{opacity:0},{opacity:1},"-=0.5");
// svgstroke.fromTo('.pictoblock',2,{opacity:0},{opacity:1},"-=0.5");

// svgstroke.to('.strokeman',2,{strokeDashoffset:0, ease: Linear.easeNone},"-=0.5");
// svgstroke.to('.arcman',2,{strokeDashoffset:0, ease: Linear.easeNone},"-=0.5");
// svgstroke.to('.cravate',2,{strokeDashoffset:0, ease: Linear.easeNone},"-=0.5");
// svgstroke.fromTo('.yellowman',2,{opacity:0},{opacity:1},"-=0.5");


// var scene4 = new ScrollMagic.Scene({triggerElement: ".fonctionnality-block",duration: 200,offset:-200})
//     .setTween(svgstroke)
//     .addTo(controller);


// if ($(window).innerWidth() < 768) {
//     $(".landing-arrow").css('display','none');
//     $(window).scroll(function () {
//         $(".landing-arrow").attr("style", "display: none !important");
//     });
//     $('.landing-arrow').addClass('none');
// }

// if ($(window).innerWidth() >768) {
//     $('.landing-arrow').removeClass('none');
// }

// function scrollarrow(){
//     $(window).scroll(function() {
//         if ($(this).scrollTop()>200)
//         {
//             $('.landing-arrow').fadeOut();
//         }
//         else
//         {
//             $('.landing-arrow').fadeIn();
//         }
//     });
// }
// $(window).resize(function() {
//     if ($(window).width() < 768) {
//         $(".landing-arrow").css('display','none');
//         $(window).off('scroll');
//     }
// });

// $(window).resize(function() {
//     if ($(window).width() > 768) {
//         $(".landing-arrow").css('display','block');
//         $(window).on('scroll');
//         scrollarrow();
//     }
// });

// scrollarrow();

// $(".landing-arrow").click(function() {
//     arrowclick();
// });
// $(".landing-arrowsmall").click(function() {
//     arrowclickResponsive();
// });

// function arrowclick(){
//     $('html,body').animate({
//             scrollTop: $('#plateform').offset().top -50},
//         'slow');
// }

// function arrowclickResponsive(){
//     $('html,body').animate({
//             scrollTop: $('#plateform').offset().top},
//         'slow');
// }

// <!-- Carousel Slick -->
// $('.variable-width').slick({
//     dots: true,
//     infinite: true,
//     speed: 300,
//     slidesToShow: 1,
//     dots: true,
//     centerMode: true,
//     variableWidth: true
// });

// console.log('%cBienvenue sur la console !', 'font-size: 30px; color: #D13272');
// console.log("%cSi vous voyez des bugs qui s'affichent ici c'est juste des features volontaires!", 'font-size: 15px; color: #0198E1');

window.addEventListener("load", function(){
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#e30c74",
                "text": "#ffffff"
            },
            "button": {
                "background": "#face07"
            }
        },
        "theme": "classic",
        "position": "bottom-right",
        "content": {
            "message": "Ce site utilise des cookies pour vous garantir une expérience utilisateur optimale.",
            "dismiss": "J'accepte !",
            "link": "En savoir plus",
            "href": "https://www.takemyhand.fr/politique-confidentialite.html"
        }
    })});